# Individual Report for Jeremy Brooking

## Windermere Visit 

We arranged with Jacob to get a tour of the buildings so that I could take some photos as references to work off. This didnt go quite to plan as the buildings were not empty, and we didnt want to interupt classes.

I placed them in my dropbox as that gave me getter control on my devices they synced too. I shared these images with the group and we will be able to reference them for future work

https://www.dropbox.com/sh/owqoj6myffj0qlr/AAAzwktnAvWyjVtZoRT1R1-2a?dl=0

## Project Management Tools

I looked into, and installed a couple of project management tools. I have an Open Source background so wanted to steer away from that if I could and make use of my Dreamspark account and setup a SharePoint server with Microsoft Project 16 installed to use its web based project management features. This however, was just going to be too impractical as it would have relied on the project manager using their dreamspark account to install Microsoft Project as the non standard ports required would not have worked well from behind Toi-Ohomais wireless network. For this reason I decdided to go back to a purely web based project management system. 

With Tony we googled some Open Source solutions and both liked the look of OpenProject. I setup a server with an isntall of OpenProject http://project.mafia.org.nz/ however by the time I had finished installing, configuring, adding users I started to see that this application though an ideal application, was well beyond the scope of what we as a team would find usable. Regan Sam Tony signed up and we all had a look and decided a simplier solution was needed for the size and length of our project.

Feel free to create an account and login at http://project.mafia.org.nz/ to see how far overboard it ended up being.

## My Assets

The assets I have been tasked are:

J134 Breakout Room  
J135 Dissasembly Lab/Classroom  
J136 Dissasembly Lab/Classroom  
Networking Switches  
Servers  
PC's for Dissasembly  